#!/usr/bin/env bash

mkdir -p ./deployment/.generated
rm -rf deployment/.generated/*

echo "set namespace context to react-app" # конфигурируется репозиторий с namespace должен быть первый
kubectl config use-context taws-rds-psql/deploy-react:kube-react --namespace=react-app

echo "select to current namespace react-app" # выбор namespace в котором будет делатся деплой
kubectl config set-context --current --namespace=react-app

echo "generating configmap from fat.properties" 
kubectl create configmap ${APP} --from-env-file deployment/config/fat${env}.properties -o yaml --dry-run=client | envsubst > "./deployment/.generated/config-map.yaml"

echo "launch all yaml files  from tmpl ..." #   генерируется запуск файлов YAML
for f in ./deployment/tmpl/*.yaml
do
  envsubst < $f > "./deployment/.generated/$(basename $f)"
done

echo "launch kubectl apply -f all YAML files" # APPLY всех файлов YAML но они используя ENVSUBST генерируются в скрытый файл .generated и от туда запускаются
for y in ./deployment/.generated/*.yaml
do
  echo "deploying $y for react-tt ..."
  kubectl apply -f $y
done

ls ./deployment/.generated

echo "watching rollout status..."
kubectl rollout status -n "react-app" -w "deployment/react-tt"

